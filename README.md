[![Project Status: Wip - Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](http://www.repostatus.org/badges/0.1.0/wip.svg)](http://www.repostatus.org/#wip)

# autonomics.preprocess

Preprocess omics data.    
Part of [*autonomics*](https://bitbucket.org/graumannlabtools/autonomics), the R suite for automated omics data analysis.

# Installation

To install the package, you first need the
[*devtools*](https://github.com/hadley/devtools) package.

```{r}
install.packages("devtools")
```

Then you can install the *autonomics.preprocess* package using

```{r}
library(devtools)
install_bitbucket("graumannlabtools/autonomics.preprocess")
```

# Functionality

`normalize_samples`